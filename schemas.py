from pydantic import BaseModel
from typing import Optional, List


class User(BaseModel):
    name: str
    email: str
    password: str


class Blog(BaseModel):
    title: str
    body: str
    #published: Optional[bool]

    class Config():
        orm_mode = True


class UserShow(BaseModel):
    name: str
    email: str
    blogs: List[Blog]

    class Config():
        orm_mode = True


class ShowBlog(BaseModel):
    title: str
    body: str
    creator: UserShow

    class Config():
        orm_mode = True


class Login(BaseModel):
    username: str
    password: str


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: Optional[str] = None