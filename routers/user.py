from typing import List
from fastapi import APIRouter, Depends, status
import models, schemas
from database import get_db
from sqlalchemy.orm import Session
from hashing import Hash


router = APIRouter(
    prefix='/router_user',
    tags=['router users'],
    )


@router.post('/', response_model= schemas.UserShow)
def create_router_user(request: schemas.User, db: Session = Depends(get_db)):
    new_user = models.User(name=request.name, email=request.email, password=Hash.bcrypt(request.password))
    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    return new_user


@router.get('/{id}', response_model=schemas.UserShow)
def get_router_user(id: int, db: Session = Depends(get_db)):
    user = db.query(models.User).filter(models.User.id==id).first()

    if not user:
        raise HTTPException(
            status_code = status.HTTP_404_NOT_FOUND, 
            detail = f'User with the id {id} is not found'
            )
    return user