from typing import List
from fastapi import APIRouter, Depends, status, Response
import models, schemas, oauth2
from database import get_db
from sqlalchemy.orm import Session
import repository.blog as repo_blog


router = APIRouter(
    prefix='/router_blog',
    tags=['router blogs'],
    )

''' Using a repo functions and required authentication '''
@router.get('/', response_model=List[schemas.ShowBlog]) # response_model es el objeto que se retorna
def all(db: Session = Depends(get_db), current_user: schemas.User = Depends(oauth2.get_current_user)):
    blogs = repo_blog.get_all(db)
    return blogs


''' Show Blog action using responsive model '''
@router.get('/{id}', status_code=200, response_model=schemas.ShowBlog)
def show(id: int, response: Response, db: Session = Depends(get_db)):
    blog = db.query(models.Blog).filter(models.Blog.id==id).first() # yes, it is with ==
    if not blog:
        raise HTTPException(
            status_code = status.HTTP_404_NOT_FOUND, 
            detail = f'Blog with the id {id} is not available'
            )
        ''' the raise is the same the next two lines
        response.status_code = status.HTTP_404_NOT_FOUND
        return {'detail': f'Blog with the id {id} is not available'} 
        '''
    return blog


@router.post('/', status_code=status.HTTP_201_CREATED)
def create(blog: schemas.Blog, db: Session = Depends(get_db)):
    new_blog = models.Blog(title=blog.title, body=blog.body, user_id=1)
    db.add(new_blog)
    db.commit()
    db.refresh(new_blog)
    return new_blog


@router.delete('/{id}', status_code=status.HTTP_204_NO_CONTENT)
def destroy(id: int, response: Response, db: Session = Depends(get_db)):
    blog = db.query(models.Blog).filter(models.Blog.id==id)

    if not blog.first():
        raise HTTPException(
            status_code = status.HTTP_404_NOT_FOUND, 
            detail = f'Blog with the id {id} is not found'
            )
    blog.delete(synchronize_session=False)
    db.commit()

    return 'Done'

@router.put('/{id}', status_code=status.HTTP_202_ACCEPTED)
def update(id, blog: schemas.Blog, db: Session = Depends(get_db)):
    blog_db = db.query(models.Blog).filter(models.Blog.id==id)

    if not blog_db.first():
        raise HTTPException(
            status_code = status.HTTP_404_NOT_FOUND, 
            detail = f'Blog with the id {id} is not found'
            )
    blog_db.update(blog)
    db.commit()

    return 'Done'
