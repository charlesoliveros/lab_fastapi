from passlib.context import CryptContext


class Hash():

    def bcrypt(password: str):
        pwd_cxt = CryptContext(schemes=['bcrypt'], deprecated='auto')
        return pwd_cxt.hash(password)

    def verify(hashed_password, plain_password):
        pwd_cxt = CryptContext(schemes=['bcrypt'], deprecated='auto')
        return pwd_cxt.verify(plain_password, hashed_password)