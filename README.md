# Lab FastAPI
---

# Description
A simple API with FastAPI, pydantic and SQLAlchemy

# Run program:
```sh
uvicorn main:app --reload
```