from fastapi import FastAPI, Depends, status, Response, HTTPException
from typing import Optional, List
import uvicorn
import schemas, models
from database import engine, get_db
from sqlalchemy.orm import Session
from hashing import Hash
from routers import blog as routers_blog, user as routers_user, authentication

app = FastAPI()

models.Base.metadata.create_all(engine)

''' Path example using API Router '''
app.include_router(authentication.router)
app.include_router(routers_blog.router)
app.include_router(routers_user.router)


@app.get('/')
def index():
    return {'data':{'name':'Fast Api Examples, go to /docs ! '}}


''' Expamples using a Blog database '''

@app.get('/blog', tags=['blogs'])
def all(db: Session = Depends(get_db)):
    blogs = db.query(models.Blog).all()
    return blogs

@app.get('/model_blog', response_model=List[schemas.ShowBlog], tags=['blogs (responsive model)']) # response_model es el objeto que se retorna
def model_all(db: Session = Depends(get_db)):
    blogs = db.query(models.Blog).all()
    return blogs

@app.get('/blog/{id}', status_code=200, tags=['blogs'])
def show(id: int, response: Response, db: Session = Depends(get_db)):
    blog = db.query(models.Blog).filter(models.Blog.id==id).first() # yes, it is with ==
    if not blog:
        raise HTTPException(
            status_code = status.HTTP_404_NOT_FOUND, 
            detail = f'Blog with the id {id} is not available'
            )
        ''' the raise is the same the next two lines
        response.status_code = status.HTTP_404_NOT_FOUND
        return {'detail': f'Blog with the id {id} is not available'} 
        '''
    return blog

''' Show Blog action using responsive model '''
@app.get('/model_show/{id}', status_code=200, response_model=schemas.ShowBlog, tags=['blogs (responsive model)'])
def model_show(id: int, response: Response, db: Session = Depends(get_db)):
    blog = db.query(models.Blog).filter(models.Blog.id==id).first() # yes, it is with ==
    if not blog:
        raise HTTPException(
            status_code = status.HTTP_404_NOT_FOUND, 
            detail = f'Blog with the id {id} is not available'
            )
        ''' the raise is the same the next two lines
        response.status_code = status.HTTP_404_NOT_FOUND
        return {'detail': f'Blog with the id {id} is not available'} 
        '''
    return blog


@app.post('/blog', status_code=status.HTTP_201_CREATED, tags=['blogs'])
def create(blog: schemas.Blog, db: Session = Depends(get_db)):
    new_blog = models.Blog(title=blog.title, body=blog.body, user_id=1)
    db.add(new_blog)
    db.commit()
    db.refresh(new_blog)
    return new_blog

@app.delete('/blog/{id}', status_code=status.HTTP_204_NO_CONTENT, tags=['blogs'])
def destroy(id: int, response: Response, db: Session = Depends(get_db)):
    blog = db.query(models.Blog).filter(models.Blog.id==id)

    if not blog.first():
        raise HTTPException(
            status_code = status.HTTP_404_NOT_FOUND, 
            detail = f'Blog with the id {id} is not found'
            )
    blog.delete(synchronize_session=False)
    db.commit()

    return 'Done'

@app.put('/blog/{id}', status_code=status.HTTP_202_ACCEPTED, tags=['blogs'])
def update(id, blog: schemas.Blog, db: Session = Depends(get_db)):
    blog_db = db.query(models.Blog).filter(models.Blog.id==id)

    if not blog_db.first():
        raise HTTPException(
            status_code = status.HTTP_404_NOT_FOUND, 
            detail = f'Blog with the id {id} is not found'
            )
    blog_db.update(blog)
    db.commit()

    return 'Done'


@app.post('/user', response_model= schemas.UserShow, tags=['users'])
def create_user(request: schemas.User, db: Session = Depends(get_db)):
    new_user = models.User(name=request.name, email=request.email, password=Hash.bcrypt(request.password))
    db.add(new_user)
    db.commit()
    db.refresh(new_user)
    return new_user


@app.get('/user/{id}', response_model=schemas.UserShow, tags=['users'])
def get_user(id: int, db: Session = Depends(get_db)):
    user = db.query(models.User).filter(models.User.id==id).first()

    if not user:
        raise HTTPException(
            status_code = status.HTTP_404_NOT_FOUND, 
            detail = f'User with the id {id} is not found'
            )
    return user



''' Basic examples '''

'''
@app.get('/blog_example')
def blog_example(limit=10, published: bool=True, sort: Optional[str] = None): # query params
    if published:
        return {'data':{'name': f'{limit} Published Blogs list'}}

    return {'data':{'name': f'{limit} Unpublished Blogs list'}}


@app.get('/blog/unpublished')
def unpublished():
    return {'data': 'all the unpublished blogs'}


@app.get('/blog_example/{id}')
def show_blog_eblog_examplexample(id: int): # path params
    return {'data': id}


@app.get('/blog/{id}/comments')
def comments(id):
    return {'data': {'1','2'}}

'''


''' if you remove the next as a comment, you will be able to run the app with the command "python main.py" 
    else, you can run the app with the command uvicorn main.app --reload'' 
'''
'''if __name__ == '__main__':
    uvicorn.run(app, host='localhost', port=8001)'''